package edu.gmu.isa562;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by DavidIgnacio on 4/4/2016.
 */
public class Alice {
    public void start(String hostName, int portNumber, String secret) {
        try{
            Socket clientSocket = new Socket(hostName, portNumber);

            System.out.println("Alice started communication ");
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));

            String message01 = "Hi Bob";
            String encrypted = encrypt(message01, secret);
            System.out.println("Sending: " + encrypted);
            out.println(encrypt(message01, secret));
            out.flush();

            clientSocket.close();
            System.out.println("Alice ended communication ");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String encrypt(String message, String secret) {
        StringBuffer output = new StringBuffer();
        if(message == null){
            return "";
        }
        for(char c: message.toCharArray()){
            output.append(c + secret);
        }

        return output.toString();
    }
}
