package edu.gmu.isa562;

public class Main {

    public static void main(String[] args) {

        String hostName = "localhost";
        int portNumber = 7777;

        String secret = "secret";
        try {

            hostName = args[0];
            portNumber = Integer.parseInt(args[1]);
            secret = args[2];
        }catch (Exception e){
            System.out.println("Using default port 7777");
        }
        Alice alice = new Alice();
        alice.start(hostName, portNumber, secret);
    }
}
