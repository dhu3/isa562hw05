package edu.gmu.isa562;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by DavidIgnacio on 4/4/2016.
 */
public class Bob {
    public void start(int portNumber, String secret) {
        System.setProperty("javax.net.ssl.trustStore", "../keystore.jks");
        System.setProperty("javax.net.ssl.keyStore",

        try {
            System.out.println("Bob waiting for communication ");
            ServerSocket serverSocket = new ServerSocket(portNumber);
            Socket clientSocket = serverSocket.accept();

            System.out.println("Bob started communication ");
            PrintWriter out =
                    new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));

            String inputLine, outputLine;
            while ((inputLine = in.readLine()) != null) {
                outputLine = decrypt(inputLine, secret);
                System.out.println("Receiving " + outputLine);
                break;
            }
            clientSocket.close();
            serverSocket.close();
            System.out.println("Bob ended communication ");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String decrypt(String inputLine, String secret) {
        return inputLine.replaceAll(secret, "");
    }
}
