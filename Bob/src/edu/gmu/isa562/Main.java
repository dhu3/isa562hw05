package edu.gmu.isa562;

public class Main {

    public static void main(String[] args) {
        int portNumber = 7777;
        String secret = "secret";
        try {
            portNumber = Integer.parseInt(args[0]);
            secret = args[1];
        }catch (Exception e){
            System.out.println("Using default port 7777 and secret \"secret\"");
        }
	    Bob bob = new Bob();
        bob.start(portNumber, secret);
    }
}
